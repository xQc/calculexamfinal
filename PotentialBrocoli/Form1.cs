﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PotentialBrocoli
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void btnCalculer_Click(object sender, EventArgs e)
        {
            float iNoteActuel;
            float iValeurExam;
            float iCalculNote;

            bool bValeurExamValide = float.TryParse(txtValeurExamFinal.Text, out iValeurExam);
            bool bValeurNoteValide = float.TryParse(txtNoteActuelle.Text, out iNoteActuel);

            //Condition que les valeurs soient du bon type
            if(bValeurExamValide && bValeurNoteValide­)
            {
                // Condition que les chiffres fassent du sens
                if (iNoteActuel >= 0 && iNoteActuel <= 100 && iValeurExam > 0 && iValeurExam <= 100)
                {
                    //Calcul de la note que l'étudiant doit avoir
                    iCalculNote = ((6000 - iNoteActuel * (100 - iValeurExam)) / iValeurExam);

                    //Calcul si l'étudiant doit se présenter à l'exam final
                    if (iCalculNote > 100)
                    {
                        txtReponse.Text = "Tu devrais abandonner :(";
                    }
                    else if (iCalculNote <= 0)
                    {
                        txtReponse.Text = "Même pas besoin d'y aller !";
                    }
                    else
                    {
                        txtReponse.Text = iCalculNote.ToString();
                    }
                     

                }
            }
            else
            {
                MessageBox.Show("WeirdChamp");

            }
        }
    }
}
