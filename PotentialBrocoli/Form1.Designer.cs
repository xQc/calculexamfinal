﻿namespace PotentialBrocoli
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.txtValeurExamFinal = new System.Windows.Forms.TextBox();
            this.lblValeurExamFinal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoteActuelle = new System.Windows.Forms.TextBox();
            this.btnCalculer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtReponse = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtValeurExamFinal
            // 
            this.txtValeurExamFinal.Location = new System.Drawing.Point(58, 60);
            this.txtValeurExamFinal.Name = "txtValeurExamFinal";
            this.txtValeurExamFinal.Size = new System.Drawing.Size(110, 20);
            this.txtValeurExamFinal.TabIndex = 0;
            // 
            // lblValeurExamFinal
            // 
            this.lblValeurExamFinal.AutoSize = true;
            this.lblValeurExamFinal.Location = new System.Drawing.Point(62, 44);
            this.lblValeurExamFinal.Name = "lblValeurExamFinal";
            this.lblValeurExamFinal.Size = new System.Drawing.Size(106, 13);
            this.lblValeurExamFinal.TabIndex = 1;
            this.lblValeurExamFinal.Text = "Valeur de l\'exam final";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ma note actuel";
            // 
            // txtNoteActuelle
            // 
            this.txtNoteActuelle.Location = new System.Drawing.Point(58, 165);
            this.txtNoteActuelle.Name = "txtNoteActuelle";
            this.txtNoteActuelle.Size = new System.Drawing.Size(110, 20);
            this.txtNoteActuelle.TabIndex = 2;
            // 
            // btnCalculer
            // 
            this.btnCalculer.Location = new System.Drawing.Point(38, 219);
            this.btnCalculer.Name = "btnCalculer";
            this.btnCalculer.Size = new System.Drawing.Size(147, 69);
            this.btnCalculer.TabIndex = 4;
            this.btnCalculer.Text = "&Calculer ma note !";
            this.btnCalculer.UseVisualStyleBackColor = true;
            this.btnCalculer.Click += new System.EventHandler(this.btnCalculer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(246, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 26);
            this.label1.TabIndex = 5;
            this.label1.Text = "En assaumant qu\'il reste seulement \r\nl\'examen finale à faire";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(246, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Note que tu dois avoir à l\'exam final";
            // 
            // txtReponse
            // 
            this.txtReponse.Location = new System.Drawing.Point(272, 174);
            this.txtReponse.Name = "txtReponse";
            this.txtReponse.ReadOnly = true;
            this.txtReponse.Size = new System.Drawing.Size(131, 20);
            this.txtReponse.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(174, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "%";
            // 
            // frmPrincipal
            // 
            this.AcceptButton = this.btnCalculer;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 308);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtReponse);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCalculer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNoteActuelle);
            this.Controls.Add(this.lblValeurExamFinal);
            this.Controls.Add(this.txtValeurExamFinal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmPrincipal";
            this.Text = "Pour Andrew";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TextBox txtValeurExamFinal;
        private System.Windows.Forms.Label lblValeurExamFinal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoteActuelle;
        private System.Windows.Forms.Button btnCalculer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtReponse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

